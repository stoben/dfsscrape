# dfsScrape


Install: [npm](https://www.npmjs.com/), [node](https://nodejs.org/en/) <br/>
Editor: [Visual studio Code](https://code.visualstudio.com/)

npm i
node dfsScrape

Scrape dfs and save results to json

Uses [Puppeteer](https://github.com/puppeteer/puppeteer)

# Todo

* Støtte for klasser med 25-skudd
* Mer robust uthenting av resultat og antall inner (etter hvordan resultater er registrert på dfs)
* Støtte for å hente flere stevner i samme kjøring
* m.m.