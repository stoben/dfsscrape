const puppeteer = require('puppeteer'); //npm i puppeteer
const fs = require('fs');


//BrukerID for dfs-bruker
const user = ""; //set user and password to use here, only necessary for catching shooter profile and ID 
const pw = "";


//set event here
const eventid = "193199"
const eventname = "Eidsbygda"

//classes that is to be extracte
const classes = ['J','ER','R','EJ','1','2','3','4','5','V55','V65','V73']
//OBS - klasse 1 skyter bare 25 skudd, og det er ikke støtte for det

//only necessary when not headless
const width = 1400;
const height = 1400;

(async () => {
    const browser = await puppeteer.launch({headless: true, slowMo: 0,args: [
        '--disable-infobars',
        `--window-size=${ width },${ height }`
      ],});

    const page = await browser.newPage();
    await page.setViewport({width: width, height: height});

    //dump console
    page.on('load', () => console.log("Loaded: " + page.url()));
    page.on('console', msg => console.log('PAGE LOG:', msg.text()));

    //sign in    
    if(user != "" && pw != "")
    {
        await page.goto("https://www.dfs.no/mitt-dfs/logg-inn/?ReturnUrl=%2fmitt-dfs%2fpersonalia%2f%3ftab%3d5");

        await page.type("#ctl00_FullRegion_MainContentRegion_RightRegion_ctl00_ctl00_ctl01_ctl00_uxLogin_uxLogin_UserName", user);    
        await page.type("#ctl00_FullRegion_MainContentRegion_RightRegion_ctl00_ctl00_ctl01_ctl00_uxLogin_uxLogin_Password", pw);
        await page.click("#ctl00_FullRegion_MainContentRegion_RightRegion_ctl00_ctl00_ctl01_ctl00_uxLogin_uxLogin_LoginButton");
    }
    
    res = {'Stevne': eventname, 'Stevneid': eventid, 'Resultater': []};

    for(c of classes.entries())
    {
        
        var url = `https://www.dfs.no/aktuelt-na/resultater/?querystring=&eventId=${eventid}&class=${c[1]}&sortby=t&sortOrder=ASC`;
        await page.goto(url);

        
        const shooters = await page.evaluate(() => {
            var skyttere = [];
            
            var table = document.querySelector("#content > table");
            if(!table) return {}; //no results

            var profile = "";
            var navn = "";
            var skytterlag = "";
            var pos = 1;
            var point = 0;
            var inner = 0;
            var shootclass = "";
            var lastpos = 0;

            for (var i = 0, row; row = table.rows[i]; i++) {
                if(i == 0) { continue;}
                p = 0;
                var equal = false;
                
                //console.log("row " + i + " " + row.textContent)
                for (var j = 0, col; col = row.cells[j]; j++) {                    
                    
                    if(j == 1)
                    {                     
                        var nxt = col.nextSibling;
                                               
                        navn = col.textContent.trim();
                        profile = col.childNodes[1].getAttribute('href') || "";
                    }
                    else if(j == 2)
                    {
                        shootclass = col.textContent.trim();
                    }
                    else if(j == 3)
                    {
                        skytterlag = col.textContent.trim();
                    }                    
                    else if(j == 9)
                    {                        
                        var p = col.childNodes[1].textContent;
                        var curinner = 0;
                        var innernode = document.querySelector("#content > table > tbody > tr:nth-child("+ (i + 2) + ") > td > table > tbody > tr:nth-child(5) > td:nth-child(6)").textContent.trim();
                        if(innernode)
                            curinner = innernode.split(' ')[1];
                        else 
                            curinner = 0;

                        if(p === point)
                        {   
                            //disse meldingene vil komme, basert på hvordan stevnet er lagt inn med antall inner (antall inner kan angis under 25-skudd, eller sum på skytter
                            //juster ovenfor etter behov

                            if(!curinner)
                                console.log("Samme poengsum " + p + " i klasse " + shootclass + ". Ukjent antall inner "); // + i);

                            if(curinner && curinner == inner) 
                            {
                                console.log("Samme poengsum " + p + " og inner " + curinner + " i klasse " + shootclass + ". Sjekk antall inner "); // + i);
                                equal = true;
                            }
                            else
                            { equal = false  }                                                       
                            
                        }
                        else{ equal = false; }
                                                
                        inner = curinner;   
                        point = p;
                    }
                    
                    if(j >= 9)
                    {//break                        
                        if(equal === false) //do not jump if equal
                        { lastpos = pos;}
                        
                        skyttere.push({'Profile': profile, 'Skytter':navn, 'Skytterlag':skytterlag, 'Plassering': lastpos, 'Poeng': point, 'Inner': inner, 'Konkurranseklasse': shootclass});
                        
                        pos +=1; 
                        break;
                    }                    
                }
            }

            return skyttere;
        });


        resultater = { 'Klasse': c[1], 'Skyttere': shooters}
        
        res.Resultater.push(resultater);
        
    }


    //dump json to file
    fs.writeFileSync(`${eventid}.json`, JSON.stringify(res), 'utf-8');

    //
    //await page.waitFor(5000);
    await browser.close();


})();